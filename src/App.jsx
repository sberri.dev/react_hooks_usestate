import './App.css'
import Counter from './Counter'

function App() {

  return (
    <>
    <h1>Compteur</h1>
    <Counter />
      
    </>
  )
}

export default App
