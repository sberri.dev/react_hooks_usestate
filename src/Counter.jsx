import { useState } from "react";
import "./Counter.css"


export default function Counter() {
  const [count, setCount] = useState(0);

  function increaseCounter() {
    setCount(count + 1);
  }

  function decreaseCounter() {

    if (count > 0) {
      setCount(count - 1)
    }
    
  }

  function resetCounter() {
    setCount(0);
  }

  return (

    <>
   <div className="display-counter"> {count} </div>
    <div className="buttons"> 
    <button className="buttons-one" onClick={increaseCounter}> + </button>
    <button className="buttons-two" onClick={decreaseCounter}> - </button>
    <button className="buttons-three" onClick={resetCounter}> 0 </button>
    </div>
    </>
   
  );
}
